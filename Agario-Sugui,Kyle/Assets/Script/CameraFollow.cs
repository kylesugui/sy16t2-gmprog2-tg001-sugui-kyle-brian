<<<<<<< HEAD
﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{   
    public GameObject target;
    public float minOCSize;
    public float smoothTime; // Camera smooth movement variance (higher = more delay)
    
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("player");        
    }
    // Update is called once per frame
    void Update()
    {
        // Zoom out camera or increase size whenever player's scale increases.
        // 5 is the minimum size.    
        float playerScale = target.GetComponent<Player>().scale;
        GetComponent<Camera>().orthographicSize = playerScale + minOCSize;


        transform.position = Vector2.Lerp(transform.position, target.transform.position, smoothTime * Time.deltaTime); 
    } 
=======
﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public float smoothVariance;
    public float minCamOS; // Orthographic Size
    public GameObject target;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("player");
    }

    void Update()
    {
        // Zoom out camera or increase size whenever player's scale increases.
        // 5 is the minimum size.
        float playerScale = target.GetComponent<Player>().scale;
        GetComponent<Camera>().orthographicSize = playerScale + minCamOS;

        transform.position = Vector2.Lerp(transform.position, target.transform.position, Time.deltaTime * smoothVariance);
   } 
>>>>>>> 3c884829090925f65ae2015874d797eee413176b
}