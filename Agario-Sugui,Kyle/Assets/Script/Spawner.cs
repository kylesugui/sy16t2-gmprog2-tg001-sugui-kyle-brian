<<<<<<< HEAD:Agario-Sugui,Kyle/Assets/Script/Spawner.cs
﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{

    public GameObject Cell;    
    public float spawnSpeed;
    public float initSpawnCount;
    public float maxRandomSpawnX = 30;
    public float maxRandomSpawnY = 30;    
    public float maxNoOfSpawn; 

    void Start()
    {
        // Invokes the spawn method repeatedly.
        InvokeRepeating("GenerateRandomSpawn", 0, spawnSpeed);
        for (int i = 0; i < initSpawnCount; i++)
        {
            GenerateRandomSpawn();
        }
    }
    void GenerateRandomSpawn()
    {
        float x = Random.Range(-maxRandomSpawnX, maxRandomSpawnX);
        float y = Random.Range(-maxRandomSpawnY, maxRandomSpawnY);

        Vector2 spawnPosition = new Vector2(x, y);

        Instantiate(Cell, spawnPosition, Quaternion.identity);
    }
}

// Try to use deltaTime for spawning
=======
﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour
{

    public GameObject Cell;    
    public float spawnSpeed;    
    public float maxRandomSpawnX = 30;
    public float maxRandomSpawnY = 30;    
    public float maxNoOfSpawn; 

    void Start()
    {
        for (int i = 0; i < 30; i++)
        {
            GenerateRandomSpawn();
        }
    }
    void GenerateRandomSpawn()
    {
        float x = Random.Range(-maxRandomSpawnX, maxRandomSpawnX);
        float y = Random.Range(-maxRandomSpawnY, maxRandomSpawnY);

        Vector2 spawnPosition = new Vector2(x, y);

        Instantiate(Cell, spawnPosition, Quaternion.identity);
    }
}
>>>>>>> 3c884829090925f65ae2015874d797eee413176b:Agario-Sugui,Kyle/Assets/Script/SpawnManager.cs
