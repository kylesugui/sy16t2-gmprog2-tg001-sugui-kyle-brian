<<<<<<< HEAD
﻿using UnityEngine;
using System.Collections;

// Make a base class for cells if more time (foods, enemies, player)
public class Player : MonoBehaviour
{
    public float scale = 1.0f;
    public float movementSpeed = 10.0f;

    // Update is called once per frame
    void Start()
    {
        // Instantiate Player?
    }
    void Update()
    {
        PlayerMovement();
        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            scale += 0.1f;
        }
        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            scale -= 0.1f;
        }
        transform.localScale = new Vector2(scale, scale);
    } 
    void PlayerMovement()
    {
        // Get the mouse's position.
        // Move the game object towards that mouse's position.
        // Make the speed proportional to the game object's scale (by division);
        // Make cell's movement smooth when changing direction (for delay)

        Vector2 mouseLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = Vector2.MoveTowards(transform.position, mouseLocation, (movementSpeed / scale) * Time.deltaTime);
    }
=======
﻿using UnityEngine;
using System.Collections;

// Make a base class for cells if more time (foods, enemies, player)
public class Player : MonoBehaviour
{
    public float scale = 1.0f;
    public float movementSpeed = 10.0f;

    // Update is called once per frame
    void Start()
    {
        // Instantiate Player?
    }
    void Update()
    {
        PlayerMovement();
        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            scale += 0.1f;
        }
        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            scale -= 0.1f;
        }
    }

    void PlayerMovement()
    {
        // Get the mouse's position.
        // Move the game object towards that mouse's position.
        // Make the speed proportional to the game object's scale (by division);
        // Make cell's movement smooth when changing direction (for delay)
        transform.localScale = new Vector2(scale, scale);

        Vector2 mouseLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = Vector2.MoveTowards(transform.position, mouseLocation, (movementSpeed / scale) * Time.deltaTime);
    }
>>>>>>> 3c884829090925f65ae2015874d797eee413176b
}